package Estructuras;

public class ArbolBalanceado<K extends Comparable<K>,V> {
	private static final boolean RED= true;
	private static final boolean BLACK= false;
	
	private NodoArbol raiz;
	private int size;
	
	public ArbolBalanceado() {
		// TODO Auto-generated constructor stub
	}
	
	//si el nodo es rojo, false si x es nulo
	public boolean isRed(NodoArbol pNodo){
		if(pNodo==null)
			return false;
		return pNodo.darColor()==RED;
	}
	
	public int getSize(NodoArbol pNodo){
		if (pNodo==null)
			return 0;
		else
			return pNodo.getN();
	}
	
	public boolean isEmpty(){
		return size==0;
	}
	
	public V get(K pLlave){
		V ret=get(pLlave, raiz);
		return ret;
	}
	
	public V get(K pLlave, NodoArbol<K,V> pNodo){
		V ret=null;
		while(pNodo!=null){
			if(pNodo.getLlave().equals(pLlave)){
				ret=(V) pNodo.getValor();
				break;
			}
			else if(pNodo.getLlave().compareTo(pLlave)>0){
				pNodo= pNodo.getIzquierda();
			}
			else{
				pNodo= pNodo.getDerecha();
			}
		}
		return ret;
	}
	
	public void add(K pLlave, V pValor){
		raiz= add(raiz, pLlave, pValor);
		raiz.setColor(BLACK);
	}
	
	public NodoArbol<K,V> add(NodoArbol<K,V> pNodo, K pLlave, V pValor){
		NodoArbol<K,V> ret= null;
		if(pNodo==null) 
			ret= new NodoArbol<K, V>(pLlave, pValor, 0, RED);
		else if(pLlave.compareTo(pNodo.getLlave())<0){
			NodoArbol<K,V> temp=pNodo.getIzquierda();
			pNodo.setIzquierda(add(temp,pLlave,pValor));
		}
		else if(pLlave.compareTo(pNodo.getLlave())>0){
			NodoArbol<K,V> temp=pNodo.getDerecha();
			pNodo.setIzquierda(add(temp,pLlave,pValor));
		}
		else pNodo.setValor(pValor);
		
		if(isRed(pNodo.getDerecha())&&!isRed(pNodo.getIzquierda()))
			pNodo=rotarIzquierda(pNodo);
		if(isRed(pNodo.getIzquierda())&& isRed(pNodo.getIzquierda().getIzquierda()))
			pNodo=rotarDerecha(pNodo);
		if(isRed(pNodo.getIzquierda())&& isRed(pNodo.getDerecha()))
			cambiarColores(pNodo);
		pNodo.setN(size(pNodo.getIzquierda())+ size(pNodo.getDerecha())+1);
		return ret;
	}
	
	public void delete(K pLlave){
		if(!contiene(pLlave))
			return;
		//Si los dos hijos son negros, el papa es rojo
		if(!isRed(raiz.getDerecha())&& !isRed(raiz.getIzquierda()))
			raiz.setColor(RED);
		raiz= delete(raiz,pLlave);
		if(!isEmpty())
			raiz.setColor(BLACK);
	}
	
	public NodoArbol<K,V> delete(NodoArbol pNodo, K pLlave){
		if(pLlave.compareTo((K) pNodo.getLlave())<0){
			if(!isRed(pNodo.getIzquierda())&&!isRed(pNodo.getIzquierda().getIzquierda()))
				pNodo= rotarDerecha(pNodo);
			pNodo.setIzquierda(delete(pNodo.getIzquierda(), pLlave));
		}
		else{
			if(isRed(pNodo.getIzquierda()))
				pNodo= rotarDerecha(pNodo);
			if(pLlave.compareTo((K) pNodo.getLlave())==0&&pNodo.getDerecha()==null)
				return null;
			if(!isRed(pNodo.getDerecha())&&!isRed(pNodo.getDerecha().getIzquierda()))
				pNodo= moverRojoDerecha(pNodo);
			if(pLlave.compareTo((K) pNodo.getLlave())==0){
				NodoArbol<K,V> temp= min(pNodo.getDerecha());
				pNodo.setLlave(temp.getLlave());
				pNodo.setValor(temp.getValor());
				pNodo.setDerecha(deleteMin(pNodo.getDerecha()));
			}
			else pNodo.setDerecha(delete(pNodo.getDerecha(),pLlave));
		}
		return balancear(pNodo);
	}
	
	public NodoArbol<K,V> deleteMin(NodoArbol pNodo){
		if (pNodo.getIzquierda()==null)
				return null;
		if(!isRed(pNodo.getIzquierda())&&!isRed(pNodo.getIzquierda().getIzquierda()))
			pNodo= moverRojoIzquierda(pNodo);
		pNodo.setIzquierda(deleteMin(pNodo));
		return balancear(pNodo);
	}
	
	public NodoArbol<K, V> rotarDerecha(NodoArbol pNodo){
		NodoArbol<K,V> temp= pNodo.getIzquierda();
		pNodo.setIzquierda(temp.getDerecha());
		temp.setDerecha(pNodo);;
		temp.setColor(temp.getDerecha().darColor());
		temp.getDerecha().setColor(RED);
		temp.setN(pNodo.getN());
		pNodo.setN(size(pNodo.getIzquierda())+size(pNodo.getDerecha())+1);
		return temp;
	}
	
	public NodoArbol<K, V> rotarIzquierda(NodoArbol pNodo){
		NodoArbol<K,V> temp= pNodo.getDerecha();
		pNodo.setDerecha(temp.getIzquierda());
		temp.setIzquierda(pNodo);
		temp.setColor(temp.getIzquierda().darColor());
		temp.getIzquierda().setColor(RED);
		temp.setN(pNodo.getN());
		pNodo.setN(size(pNodo.getIzquierda())+size(pNodo.getDerecha())+1);
		return temp;
	}
	
	public void cambiarColores(NodoArbol pNodo){
		pNodo.setColor(!pNodo.darColor());
		pNodo.getIzquierda().setColor(!pNodo.getIzquierda().darColor());
		pNodo.getDerecha().setColor(!pNodo.getDerecha().darColor());
	}
	
	public NodoArbol<K,V> moverRojoIzquierda(NodoArbol pNodo){
		cambiarColores(pNodo);
		if(isRed(pNodo.getDerecha().getIzquierda())){
			pNodo.setDerecha(rotarDerecha(pNodo.getDerecha()));
			pNodo= rotarIzquierda(pNodo);
			cambiarColores(pNodo);
		}
		return pNodo;
	}
	
	public NodoArbol<K,V> moverRojoDerecha(NodoArbol pNodo){
		cambiarColores(pNodo);
		if(isRed(pNodo.getIzquierda().getIzquierda())){
			pNodo= rotarDerecha(pNodo);
			cambiarColores(pNodo);
		}
		return pNodo;
	}
	
	public NodoArbol<K,V> balancear(NodoArbol pNodo){
		if(isRed(pNodo.getDerecha()))
			pNodo= rotarIzquierda(pNodo);
		if(isRed(pNodo.getIzquierda())&&isRed(pNodo.getIzquierda().getIzquierda()))
			pNodo= rotarDerecha(pNodo);
		if(isRed(pNodo.getIzquierda())&&isRed(pNodo.getDerecha()))
			cambiarColores(pNodo);
		pNodo.setN(size(pNodo.getIzquierda())+size(pNodo.getDerecha())+1);
		return pNodo;
	}
	
	public K min(){
		return min(raiz).getLlave();
	}
	
	public NodoArbol<K,V> min(NodoArbol pNodo){
		if(pNodo.getIzquierda()==null)
			return pNodo;
		else return min(pNodo.getIzquierda());
	}
	
	public boolean contiene(K pLlave){
		return get(pLlave)!=null;
	}
	
	public int size(NodoArbol pNodo){
		if(pNodo==null) 
			return 0;
		return pNodo.getN();
	}
	
	public int size(K inicio, K end){
		if(inicio.compareTo(end)>0)
			return 0;
		else if (contiene(end))
			return rango(end)-rango(inicio)+1;
		else
			return rango(end)-rango(inicio);
	}
	
	public int rango(K pLlave){
		return rango(pLlave, raiz);
	}
	
	public int rango(K pLlave, NodoArbol<K,V> pNodo){
		int ret=0;
		if(pLlave.compareTo(pNodo.getLlave())<0)
			ret= rango(pLlave, pNodo.getIzquierda());
		else if(pLlave.compareTo(pNodo.getLlave())>0)
			ret= 1+ size(pNodo.getIzquierda())+rango(pLlave, pNodo.getDerecha());
		else
			ret= size(pNodo.getIzquierda());
		return ret;
	}
	
	public NodoArbol<K,V> getRaiz(){
		return raiz;
	}
}
