
import java.util.Iterator;
import java.util.NoSuchElementException;
public class MyBinaryHeap<E extends Comparable<E>> implements Iterable<E>
{
	private final static int DELTA = 50;
	
	private int size;
	private NodoHash<E>[] array;
	
	@SuppressWarnings("unchecked")
	public MyBinaryHeap() 
	{
		array = new Elemento[DELTA];
		size = 0;
	}
	
	//Clase privada que representa el iterador
		private class Iterador implements Iterator<E> 
		{
			private MyBinaryHeap<E> copia;

			public Iterador()
			{
				copia = new MyBinaryHeap<E>();
				for (int i = 0; i < size; i++) 
				{
					copia.insertar(array[i].darElemento()); 
				}
			}
			
			public E elemento()
			{
				return copia.darArray()[0].darElemento();
			}

			@Override
			public boolean hasNext() 
			{
				return copia.darArray()[0] != null;
			}

			@Override
			public E next()
			{
				if(!hasNext())
					throw new NoSuchElementException();
				
				return copia.borrarMax(); 
			}

			@Override
			public void remove() 
			{ 
			}
			

		}
	
	public Elemento<E>[] darArray()
	{
		return array;
	}
	
	@SuppressWarnings("unchecked")
	public void insertar(E a)
	{
		//Verifica si hay que aumentar el tama�o
		if(size == array.length)
		{
			Elemento<E> old[] = array;
			array = new Elemento[old.length + DELTA];
			System.arraycopy(old, 0, array, 0, old.length);
		}
		
		//Se crea el elemento
		Elemento<E> nuevo = new Elemento<E>(a);
		size++;
		
		//Se busca su posici�n correcta
		int hijo = size-1;
		int padre = (hijo%2 == 0) ? (hijo - 2)/2 : (hijo - 1)/2;
		
		while(padre != -1 && nuevo.compareTo(array[padre]) > 0)
		{
			array[hijo] = array[padre];
			hijo = padre;
			padre = (hijo%2 == 0) ? (hijo - 2)/2 : (hijo - 1)/2;
		}
	
		//Se inserta el elemento
		array[hijo] = nuevo;
	}
	
	public E borrarMax()
	{	
		 E max = array[0].darElemento();
		 exch(0, --size);	 
		 array[size] = null;
		 
		 int k = 0;
		 while (2*k +1 < size)
		 {
			 int j = 2*k +1;
			 if (j+1 < size && array[j].compareTo(array[j+1]) < 0) 
				 j++;
			
			 if (array[k].compareTo(array[j]) > 0) 
				 break;
			 
			 exch(k, j);
			 k = j;
		 }
		 return max;
	}
	
	private void exch(int pos1, int pos2)
	{
		Elemento<E> aux = array[pos1];
		array[pos1] = array[pos2];
		array[pos2] = aux;
	}
	
	public boolean vacio()
	{
		return size == 0;
	}
	
	public E max()
	{
		return size == 0 ? null : array[0].darElemento();
	}
	
	public int darTam()
	{
		return size;
	}

	@Override
	public Iterator<E> iterator() 
	{
		return new Iterador();
	}

}
