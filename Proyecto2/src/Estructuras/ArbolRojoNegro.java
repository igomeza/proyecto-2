package Estructuras;

public class ArbolRojoNegro<Key extends Comparable<Key>, Value> implements IArbolRojoNegro
{
	//Constantes que definen el color del nodo
	private static final boolean RED = true;
	private static final boolean BLACK = false;

	private Nodo raiz;

	private int size;

	//Clase privada que define el nodo
	public class Nodo
	{
		private Key key; 
		private Value val; 
		private Nodo izq; 
		private Nodo der;
		private int N; 
		boolean color; 

		public Nodo(Key pKey, Value pVal, int pN, boolean pColor)
		{
			key = pKey;
			val = pVal;
			N = pN;
			color = pColor;
		}
		
		private Value getValue()
		{
			return val;
		}
	}

	//Comprueba que el nodo sea rojo, si es null tiene que ser negro.
	private boolean esRojo(Nodo x)
	{
		return (x == null) ? false: (x.color == RED);
	}

	public int size(Nodo x)
	{		
		return (x == null) ? 0: x.N;
	}
	
	public boolean isEmpty()
	{
		return (size==0) ? true: false;
	}


	public void put(Key key, Value val){ 
		raiz = put(raiz, key, val);
		raiz.color = BLACK;
	}

	//Inserta un nodo, retorna el nodo agregado
	private Nodo put(Nodo h, Key key, Value val)
	{
		if (h == null) // Inserta normal, se asume como rojo para luego manejar dependiendo del caso
			return new Nodo(key, val, 1, RED);

		int cmp = key.compareTo(h.key);

		if (cmp < 0) 
			h.izq = put(h.izq, key, val);

		else if (cmp > 0)
			h.der = put(h.der, key, val);

		else 
			h.val = val;


		//Rota a la izquierda porque su nodo derecho es rojo
		if (esRojo(h.der) && !esRojo(h.izq))
			h = rotarIzq(h);

		if (esRojo(h.izq) && esRojo(h.izq.izq)) 
			h = rotarDer(h);

		if (esRojo(h.izq) && esRojo(h.der))
			cambiarColores(h);

		h.N = size(h.izq) + size(h.der) + 1;
		size++;

		return h;
	}

	//Rota el arbol a la izquierda para balancearlo
	private Nodo rotarIzq(Nodo h)
	{
		Nodo x = h.der;

		h.der = x.izq;
		x.izq = h;

		x.color = h.color;
		h.color = RED;

		x.N = h.N;
		h.N = 1 + size(h.izq)+ size(h.der);

		return x;
	}

	//Rota el arbol a la derecha para balancearlo
	private Nodo rotarDer(Nodo h)
	{
		Nodo x = h.izq;

		h.izq = x.der;
		x.der = h;

		x.color = h.color;
		h.color = RED;

		x.N = h.N;
		h.N = 1 + size(h.izq)+ size(h.der);

		return x;
	}

	public void cambiarColores(Nodo h)
	{

		h.color = RED;
		h.izq.color = BLACK;
		h.der.color = BLACK;
	}

	private Nodo moveRedizq(Nodo h)
	{ 
		cambiarColores(h);
		if (esRojo(h.der.izq))
		{
			h.der = rotarDer(h.der);
			h = rotarIzq(h);
		}
		return h;
	}
	public Value deleteMin()
	{
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = RED;

		raiz = deleteMin(raiz);
		if (size==0)
			raiz.color = BLACK;
		
		return raiz.val;


	}

	private Nodo deleteMin(Nodo h)
	{
		if (h.izq == null)
			return null;
		if (!esRojo(h.izq) && !esRojo(h.izq.izq))
			h = moveRedizq(h);
		h.izq = deleteMin(h.izq);

		if (esRojo(h.der))
			h = rotarIzq(h);

		return h;
	}

	public void delete(Key key)
	{
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = RED;
		raiz = delete(raiz, key);
		if (size==0)
			raiz.color = BLACK;
	}

	private Nodo moveRedDer(Nodo h)
	{ 
		cambiarColores(h);

		if (!esRojo(h.izq.izq))
			h = rotarDer(h);

		return h;
	}
	
	public Value deleteMax()
	{
		if (!esRojo(raiz.izq) && !esRojo(raiz.der))
			raiz.color = RED;
		raiz = deleteMax(raiz);
		if (size==0) 
			raiz.color = BLACK;
		return raiz.val;
	}
	
	private Nodo deleteMax(Nodo h)
	{
		if (esRojo(h.izq))
			h = rotarDer(h);
		if (h.der == null)
			return null;
		if (!esRojo(h.der) && !esRojo(h.der.izq))
			h = moveRedDer(h);
		h.der = deleteMax(h.der);

		if (esRojo(h.der))
			h = rotarIzq(h);

		return h;
	}
	
	private Nodo delete(Nodo h, Key key)
	{
		if (key.compareTo(h.key) < 0)
		{
			if (!esRojo(h.izq) && !esRojo(h.izq.izq))
				h = moveRedizq(h);
			h.izq = delete(h.izq, key);
		}
		else
		{
			if (esRojo(h.izq))
				h = rotarDer(h);
			if (key.compareTo(h.key) == 0 && (h.der == null))
				return null;
			if (!esRojo(h.der) && !esRojo(h.der.izq))
				h = moveRedDer(h);
			if (key.compareTo(h.key) == 0)
			{
				h.val = get(h.der, min(h.der).key);
				h.key = min(h.der).key;
				h.der = deleteMin(h.der);
			}
			else h.der = delete(h.der, key);
		}

		if (esRojo(h.der))
			h = rotarIzq(h);

		return h;
	}

	public Value get(Key key)
	{ 
		return get(raiz, key); 
	}
	
	private Value get(Nodo x, Key key)
	{ 
		if (x == null) 
			return null;
		
		int cmp = key.compareTo(x.key);
		
		if (cmp < 0) 
			return get(x.izq, key);
		
		else if (cmp > 0)
			return get(x.der, key);	
		
		else
			return x.val;
	}

	public Key min()
	{
		return min(raiz).key;
	}
	
	private Nodo min(Nodo x)
	{
		if (x.izq == null)
			return x;

		return min(x.izq);
	}
	
	public Nodo getRaiz(){
		return raiz;
	}
}
