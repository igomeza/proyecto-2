package Estructuras;

public class NodoArbol<K extends Comparable<K>,V> {
	private K llave;
	private V valor;
	private NodoArbol derecha, izquierda;
	private int N;
	private boolean color;
	
	public NodoArbol(K pLLave, V pValor, int pN, boolean pColor) {
		llave= pLLave;
		valor= pValor;
		N= pN;
		color= pColor;
	}
	
	public NodoArbol getDerecha(){
		return derecha;
	}
	
	public NodoArbol getIzquierda(){
		return izquierda;
	}
	
	public boolean darColor(){
		return color;
	}
	
	public V getValor(){
		return valor;
	}
	
	public K getLlave(){
		return llave;
	}
	
	public int getN(){
		return N;
	}
	
	public void setColor(boolean pColor){
		color=pColor;
	}
	
	public void setDerecha(NodoArbol pNodo){
		derecha= pNodo;
	}
	
	public void setIzquierda(NodoArbol pNodo){
		izquierda= pNodo;
	}
	
	public void setN(int pN){
		N= pN;
	}
	
	public void setValor(V pValor){
		valor= pValor;
	}
	
	public void setLlave(K pLlave){
		llave= pLlave;
	}
}
