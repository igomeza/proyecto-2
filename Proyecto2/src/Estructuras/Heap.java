package Estructuras;

import java.util.Iterator;

public class Heap<T extends Comparable<T>> implements IHeap<T>, Iterable<T>
{
	public final static int DEFAULT_SIZE= 50;
	private int size;
	private T[] elementos;
	private boolean maxHeap;
	
	public Heap(boolean pTHeap) {
		elementos= (T[]) new Comparable[DEFAULT_SIZE];
		size=0;
		maxHeap= pTHeap;
	}
	
	@Override
	public void add(T elemento) {
		if(size> elementos.length)
		{
			T[] viejo= elementos;
			elementos= (T[]) new Comparable[elementos.length+DEFAULT_SIZE];
		}
		if(elemento!=null){
			size++;
			//agarra la ultima posicion
			int i= size;
			elementos[i]= elemento;
			siftUp(i);	
		}
	}

	@Override
	public T peek() {
		T elemento=null;
		if(size!=0)
			elemento= elementos[1];
		return elemento;
	}

	@Override
	public T poll() {
		T elemento= peek();
		if(elemento!=null)
		{
			elementos[1]= elementos[size];
			elementos[size]= null;	
			size--;
			siftDown(size);
		}
		return elemento;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public void siftUp(int indice) {
		T ultimo= elementos[indice];
		T temp;
		int ipadre; 
		if(ultimo!=null)
		{
			ipadre= indicePadre(indice);
			if(ipadre==-1)
				temp= null;
			else temp= elementos[ipadre];
			if(temp!= null && maxHeap)
			{
				if(temp.compareTo(ultimo)<0)
				{
					T aux=elementos[ipadre];
					elementos[ipadre]=elementos[indice];
					elementos[indice]=aux;	
					siftUp(ipadre);
				}
			}
			else if(temp!= null && !maxHeap){
				if(temp.compareTo(ultimo)>0)
				{
					T aux=elementos[ipadre];
					elementos[ipadre]=elementos[indice];
					elementos[indice]=aux;		
					siftUp(ipadre);
				}
			}
		}
	}

	@Override
	public void siftDown(int indice) {
		T padre= elementos[indice];
		T mayor;
		if(indiceHijoDer(indice)!=-1 && indiceHijoIzq(indice)!=-1)
		{
			T hijoDer= elementos[indiceHijoDer(indice)];
			T hijoIzq= elementos[indiceHijoIzq(indice)];
			int ipadre;
			if(padre!=null && hijoDer!=null && hijoIzq!=null)
			{
				ipadre= indicePadre(indice);
				int imayor;
				if(maxHeap)
				{
					if(hijoDer.compareTo(hijoIzq)<0){
						mayor= hijoIzq;
						imayor= indiceHijoIzq(indice);
					}
					else {
						mayor= hijoDer;
						imayor= indiceHijoDer(indice);
					}
					if(mayor.compareTo(padre)>0)
					{
						T temp= elementos[ipadre];
						elementos[ipadre]= mayor;
						elementos[imayor]= temp;
					}
					siftDown(ipadre);					
				}else{
					if(hijoDer.compareTo(hijoIzq)>0){
						mayor= hijoIzq;
						imayor= indiceHijoIzq(indice);
					}
					else {
						mayor= hijoDer;
						imayor= indiceHijoDer(indice);
					}
					if(mayor.compareTo(padre)<0)
					{
						T temp= elementos[ipadre];
						elementos[ipadre]= mayor;
						elementos[imayor]= temp;
					}
					siftDown(ipadre);
				}
			}
		}
	}
	
	public int indiceHijoIzq(int nodeI){
		if(nodeI>size/2)
			return -1;
		return 2*nodeI+1;
	}
	
	public int indiceHijoDer(int nodeI){
		if(nodeI>size-1/2)
			return -1;
		return 2*nodeI+2;
	}
	
	public int indicePadre(int nodeI){
		if(nodeI<0)
			return -1;
		return (nodeI-1)/2;
	}
	
	public boolean esHoja(int nodeI){
		return nodeI>=size/2 && nodeI<size;
	}
	
	public Iterator<T> iterator() 
	{
		return new HeapIterador();
	}
	
	public T[] darElementos()
	{
		return elementos;
	}
	
	public class HeapIterador implements Iterator<T>
	{
		Heap<T> copia;
		
		public HeapIterador() {
			boolean holi= maxHeap;
			copia= new Heap<T>(holi);
			for (int i = 0; i < size; i++) {
				copia.add(elementos[i]);
			}
		}
		@Override
		public boolean hasNext() {
			return copia.elementos[0]!=null;
		}

		@Override
		public T next() {
			return copia.poll();
		}
		
	}
	
	public String toString(){
		for(Comparable a:this.darElementos()){
			if(a!=null)
				System.out.println(a);
		}
		return "";
	}
}
