package Estructuras;

import Estructuras.ArbolRojoNegro.Nodo;

public interface IArbolRojoNegro<Key extends Comparable<Key>, Value> {
	
	public boolean isEmpty();
	public Value deleteMin();
	public Value deleteMax();
	public Key min();
}
