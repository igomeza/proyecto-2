package Estructuras;

public interface ITablaHash<K extends Comparable<K> ,V>
{
	public void put(K llave,V valor);
	public V get(K llave);
	public V delete(K llave);
	public float calcularFactorCarga();
	public int darNumEl();
}
