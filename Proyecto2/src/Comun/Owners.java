package Comun;

public class Owners implements Comparable<Owners>
{
	private String apellidos;
	private String nombres;
	private int cedula;
	private double direccionLatitud;
	private double direccionLongitud;
	private int [] codigoCollares;
	private String email;
	private int celular;
	private int prioridad;
	
	public Owners(String pApellido, String pNombre, int pCedula, double pDccLatitud, double pDccLongitud, int[] pCollares, String pEmail, int pCelular, int pPrioridad) 
	{
		apellidos= pApellido;
		nombres= pNombre;
		cedula= pCedula;
		direccionLatitud= pDccLatitud;
		direccionLongitud= pDccLongitud;
		codigoCollares= pCollares;
		email= pEmail;
		celular= pCelular;
		prioridad= pPrioridad;
	}
	
	public String getApellidos(){
		return apellidos;
	}
	
	public String getNombres(){
		return nombres;
	}
	
	public int getCedula(){
		return cedula;
	}
	
	public double getDireccionLatitud(){
		return direccionLatitud;
	}
	
	public double getDireccionLongitud(){
		return direccionLongitud;
	}
	
	public int[] getCodigoCollares(){
		return codigoCollares;
	}
	
	public String getEmail(){
		return email;
	}
	
	public int getCelular(){
		return celular;
	}
	
	public int getPrioridad(){
		return prioridad;
	}
	
	public String toString(){
		return "" + cedula; 
	}

	@Override
	public int compareTo(Owners o) {
		int ret=-1;
		if(this.getCedula()>o.getCedula())
			ret=1;
		else if(this.getCedula()==o.getCedula())
			ret=0;
		return ret;
	}
}
