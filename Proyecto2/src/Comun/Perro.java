package Comun;

public class Perro implements Comparable<Perro>
{
	private int numCollar;
	private String nombre;
	private String raza;
	private long edadMeses;
	private long pulsoMax;
	private long pulsoNormalInf;
	private long pulsoNormalSup;
	private long pulsoMinInf;
	private long pulsoMinSup;
//	private long zonaL;
	private Zona zona;
	private int cantAlarmasFrec;
	private int cantAlarmasZona;
	private int cantAlarmasTotal;
//	private int prioridad;
	
	public Perro(int pNumeroCollar, String pNombre, String pRaza, int pEdad, int pPulsoMax, int pPulsoNInf, int pPulsoNSup, int pPulsoMInf, int pPulsoMSup, Zona pZona) 
	{
		numCollar=pNumeroCollar;
		nombre= pNombre;
		raza= pRaza;
		edadMeses= pEdad;
		pulsoMax= pPulsoMax;
		pulsoNormalInf= pPulsoNInf;
		pulsoNormalSup= pPulsoNSup;
		pulsoMinInf=pPulsoMInf;
		pulsoMinSup= pPulsoMSup;
//		zonaL= pZonaL;
		zona= pZona;
		cantAlarmasFrec = 0;
		cantAlarmasZona = 0;
		cantAlarmasTotal = 0;
//		prioridad= pPrioridad;
	}

	public int getNumCollar() {
		return numCollar;
	}

	public String getNombre(){
		return nombre;
	}
	
	public String getRaza(){
		return raza;
	}
	
	public long getEdadMeses(){
		return edadMeses;
	}
	
	public long getPulsoMax(){
		return pulsoMax;
	}
	
	public long getPulsoNormalInf(){
		return pulsoNormalInf;
	}
	
	public long getPulsoNormalSup(){
		return pulsoNormalSup;
	}
	
	public long getPulsoMinInf(){
		return pulsoMinInf;
	}
	
	public long getPulsoMinSup(){
		return pulsoMinSup;
	}
	
	public Zona getZona(){
		return zona;
	}
	
//	public long getZonaL(){
//		return zonaL;
//	}
	public int getCantidadAlarmasFrec()
	{
		return cantAlarmasFrec;
	}
	public void actualizarCantidadAlarmasFrec()
	{
		cantAlarmasFrec++;
	}
	public int getCantidadAlarmasZona()
	{
		return cantAlarmasZona;
	}
	public void actualizarCantidadAlarmasZona()
	{
		cantAlarmasZona++;
	}
	public int getCantidadAlarmasTotales()
	{
		cantAlarmasTotal = cantAlarmasFrec+cantAlarmasZona;
		return cantAlarmasTotal;
	}
	
//	public int getPrioridad(){
//		return prioridad;
//	}
	
	public String toString(){
		return numCollar+"";
	}

	@Override
	public int compareTo(Perro o) {
		int ret=-1;
		if(getNumCollar()>o.getNumCollar())
			ret=1;
		else if(getNumCollar()==o.getNumCollar())
			ret=0;
		return ret;
	}
}
