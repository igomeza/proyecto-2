package Comun;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import Comun.AdministradorAnalisis.Rutas;
import Estructuras.*;

public class AdministradorAnalisis {
	
	//Atributos
	private Heap<Evento> perrosFrecuencia;
	private Heap<Evento> perrosDistancia;
	private TablaHash<Integer, Owners> owners;
	private TablaHash<Integer, Perro> perros;
	private Heap<Zona> zonas;
	private ArbolRojoNegro<Integer, Evento> eventos;
	
	
	public enum Rutas
	{
		AREASP("./data/Archivo_JSON_Proyecto_2/areas.json"),
		PERROS("./data/Archivo_JSON_Proyecto_2/dogs.json"),
		OWNERS("./data/Archivo_JSON_Proyecto_2/owners.json"),
		LUNES("./data/Archivo_JSON_Proyecto_2/monday.json"),
		MARTES("./data/Archivo_JSON_Proyecto_2/tuesday.json"),
		MIERCOLES("./data/Archivo_JSON_Proyecto_2/wednesday.json"),
		JUEVES("./data/Archivo_JSON_Proyecto_2/thursday.json"),
		VIERNES("./data/Archivo_JSON_Proyecto_2/friday.json");

		private String Ruta;
		private Rutas(String pRuta){
			this.Ruta= pRuta;
		}
		public String getRuta(){
			return Ruta;
		}
	}
	
	/**
	 * Parser que permite convetir a objetos JSON
	 */
	private JsonParser parser;
	
	/**
	 * Carga los archivos JSON
	 * @param pParser
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 * @throws FileNotFoundException
	 */
	public void cargarArchivos(JsonParser pParser) throws JsonIOException, JsonSyntaxException, FileNotFoundException
	{
		for(Rutas a: Rutas.values())
		{
			Rutas actual= a;
			JsonArray obj= (JsonArray) parser.parse(new FileReader(a.getRuta()));
			switch(a)
			{
			case AREASP:
				leerArea(obj);
				break;
			case PERROS:
				leerPerro(obj);
				break;
			case LUNES:
				leerEvento(obj);
				break;
			case MARTES:
				leerEvento(obj);
				break;
			case MIERCOLES:
				leerEvento(obj);
				break;
			case JUEVES:
				leerEvento(obj);
				break;
			case VIERNES:
				leerEvento(obj);
				break;
			case OWNERS:
				leerOwner(obj);
				break;
			}
		}
	}

	/**
	 * Lee los archivos JSON de due�os
	 * @param obj
	 */
	public void leerOwner(JsonArray obj) 
	{
		for (Object o: obj)
		{
			JsonObject area= (JsonObject) o;
			String apellidos= area.get("apellidos").getAsString();
			String nombres= area.get("nombres").getAsString();
			int cedula= area.get("cedula").getAsInt();
			double DLatitud= area.get("direccion_latitud").getAsDouble();
			double DLongitud= area.get("direccion_longitud").getAsDouble();
			JsonArray collares= area.get("codigo_collar").getAsJsonArray();
			int[] codCollares= new int[collares.size()];
			int i=0;
			while(i<collares.size()){
				int actual= collares.get(i).getAsInt();
				codCollares[i]= actual;
				i++;
			}
			String email= area.get("email").getAsString();
			int celular= area.get("celular").getAsInt();
			int prioridad= area.get("prioridad").getAsInt();
			Owners temp= new Owners(apellidos, nombres, cedula, DLatitud, DLongitud, codCollares, email, celular, prioridad);
			//TODO: agregarla a lo que sea
			for (int j = 0; j < codCollares.length; j++) {
				owners.put(codCollares[j], temp);
			}
		}
	}

	/**
	 * Lee los archivos JSON de los eventos
	 * @param obj
	 */
	public void leerEvento(JsonArray obj) 
	{
		for (Object o: obj)
		{
			JsonObject area= (JsonObject) o;
			int collar= area.get("codigo_collar").getAsInt();
			long fecha= area.get("fecha_hora").getAsLong();
			int ritmo= area.get("ritmo_Cardiaco").getAsInt();
			double ubicacionLatitud= area.get("ubicacion_actual_latitud").getAsDouble();
			double ubicacionLongitud= area.get("ubicacion_actual_longitud").getAsDouble();
			String pasoPuerta= area.get("paso_puerta").getAsString();
			int pasoPuertafinal=0;
			if(pasoPuerta.equals("salio"))
				pasoPuertafinal=1;
			else if(pasoPuerta.equals("llego"))
				pasoPuertafinal=-1;
			int prioridadC= buscarPrioridadD(collar);
			Evento nuevo= new Evento(collar, fecha, ritmo, ubicacionLatitud, ubicacionLongitud, pasoPuertafinal,prioridadC);
			//TODO: agregarla a lo que sea
			perrosFrecuencia.add(nuevo);
		}
	}

	/**
	 * Lee los archivos JSON de los perros
	 * @param obj
	 */
	public void leerPerro(JsonArray obj) 
	{
		for (Object o: obj)
		{
			JsonObject area= (JsonObject) o;
			int collar= area.get("codigo_collar").getAsInt();
			String nombre= area.get("nombre").getAsString();
			String raza= area.get("raza").getAsString();
			int edad= area.get("edad_meses").getAsInt();
			int pulsoMax= area.get("pul_max_pormin").getAsInt();
			int pulsoNormalInf= area.get("pul_normal_pormin_inferior").getAsInt();
			int pulsoNormalSup= area.get("pul_normal_pormin_superior").getAsInt();
			int pulsoMinInf= area.get("pul_min_pormin_inferior").getAsInt();
			int pulsoMinSup= area.get("pul_min_pormin_superior").getAsInt();
			int zona=area.get("zona").getAsInt();
			Zona Zona= buscarZonaP(zona);
			Perro nuevo= new Perro(collar, nombre, raza, edad, pulsoMax, pulsoNormalInf, pulsoNormalSup, pulsoMinInf, pulsoMinSup, Zona);
			//TODO: agregarla a lo que sea
			perros.put(collar, nuevo);
		}
	}

	/**
	 * Lee los archivos JSON de areas
	 * @param obj
	 */
	public void leerArea(JsonArray obj)
	{
		for (Object o: obj) 
		{
			JsonObject area= (JsonObject) o;
			int id= area.get("zona").getAsInt();
			String nombreP= area.get("paseador_nombre").getAsString();
			String apellidoP= area.get("paseador_apellido").getAsString();
			double ubicacionNOccLat= area.get("ubicacion_max_noroccidente_latitud").getAsDouble();
			double ubicacionNOccLong= area.get("ubicacion_max_noroccidente_longitud").getAsDouble();
			double ubicacionSOrLat= area.get("ubicacion_max_suroriente_latitud").getAsDouble();
			double ubicacionSOrLong= area.get("ubicacion_max_suroriente_longitud").getAsDouble();
			Zona nueva= new Zona(id, nombreP, apellidoP, ubicacionNOccLat, ubicacionNOccLong, ubicacionSOrLat, ubicacionSOrLong);
			//TODO: agregarla a lo que sea
			zonas.add(nueva);
		}
	}
	
	//Constructor
	public AdministradorAnalisis() {
		owners= new TablaHash<Integer, Owners>();
		perros= new TablaHash<Integer, Perro>();
		zonas= new Heap<Zona>(false);
		perrosFrecuencia= new Heap<Evento>(false);
	}
	
	//Parte A
	public void localizarFrecuenciaCardiaca(Evento pEvento){
		
	}
	
	public void consultarAlarmasCardiacas(Date pFecha){
		
	}
	
	//Parte B
	public void localizarFueraDeZona(Evento pEvento){
		
	}
	
	public void consultarAlarmasZona(Date pFecha){
		
	}
	
	//Parte C
	public void darPerrosAlarmasT(){
		
	}
	
	public void darPerrosPrioridadN(){
		
	}
	
	//Metodos axiliares
	/**
	 * Busca la prioridad de un due�o dado el numero de collar
	 * @param pCollar
	 * @return
	 */
	public int buscarPrioridadD(int pCollar){
		Owners buscado= owners.get(pCollar);
		return buscado.getPrioridad();
	}
	/**
 	 * Busca una zona dado el numero de idenficacion
 	 ** @param pNumero
	 * @return
 	 */
 	public Zona buscarZonaP(long pNumero)
 	{
 		Iterator it= zonas.iterator();
 		Zona buscada=null;
 		while(it.hasNext())
 		{
 			Zona actual= (Zona) it.next();
 			if(actual.getId()==pNumero)
 			{
 				buscada=actual;
 			}
 		}
 		return buscada;
 	}
	
}
