package Comun;

public class Zona implements Comparable<Zona>
{
	private int id;
	private String nomPaseador;
	private String apellidoPaseador;
	private double maxNOccLatitud;
	private double maxNOccLongitud;
	private double maxSOrLatitud;
	private double maxSOrLongitud;
	
	public Zona(int pId, String pNombre, String pApellido, double pNOccLatitud, double pNOccLongitud, double pSOrLatitud, double pSOrLongitud) 
	{
		id= pId;
		nomPaseador= pNombre;
		apellidoPaseador= pApellido;
		maxNOccLatitud= pNOccLatitud;
		maxNOccLongitud= pNOccLongitud;
		maxSOrLatitud= pSOrLatitud;
		maxSOrLongitud= pSOrLongitud;
	}
	
	public int getId(){
		return id;
	}
	
	public String getNomPaseador(){
		return nomPaseador;
	}
	
	public String getApellidoPaseador(){
		return apellidoPaseador;
	}
	
	public double getMaxNOccLatitud(){
		return maxNOccLatitud;
	}
	
	public double getMaxNoccLongitud(){
		return maxNOccLongitud;
	}
	
	public double getMaxSOrLatitud(){
		return maxSOrLatitud;
	}
	
	public double getMaxSOrLongitud(){
		return maxSOrLongitud;
	}
	
	public String toString(){
		return ""+ id;
	}

	@Override
	public int compareTo(Zona o) 
	{
		int ret=-1;
		if(this.id>o.getId())
			ret=1;
		else if(this.getId()==o.getId())
			ret=0;
		return ret;
	}
}
