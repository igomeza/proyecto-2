package Test;

import junit.framework.TestCase;

import Estructuras.TablaHash;

public class TablaHashTest extends TestCase{
	//Tabla de pruebas
	private TablaHash<Integer, Integer> tabla;
	private int numel;
	
	//Tabla vacia
	public void setupEscenario1()
	{
		tabla= new TablaHash<Integer, Integer>();
	}
	
	//Tabla con 50 datos
	public void setupEscenario2()
	{
		tabla= new TablaHash<Integer, Integer>();
		for (int i = 0; i < 50; i++) {
			tabla.put(i, i);
		}
	}
	
	//Tabla con 100 datos
	public void setupEscenario3()
	{
		tabla= new TablaHash<Integer, Integer>();
		for (int i = 0; i < 100; i++) {
			tabla.put(i, i);
		}
	}
	
	public void testEmpty()
	{
		setupEscenario1();
		assertEquals("La lista no esta vacia",10, tabla.Size);
	}
	
	public void testAdd1()
	{
		setupEscenario2();
		assertEquals("La lista no contiene 50 elementos",50, tabla.darNumEl());
	}
	
	public void testAdd2()
	{
		setupEscenario3();
		assertEquals("La lista no contiene 100 elementos",100, tabla.darNumEl());
	}
	
	public void testBuscar1()
	{
		setupEscenario2();
		int buscado=49;
		int agarrado= tabla.get(buscado);
		assertEquals("No se encontro el numero buscado",buscado, agarrado);
	}
	
	public void testBuscar2()
	{
		setupEscenario3();
		int buscado=73;
		int agarrado= tabla.get(buscado);
		assertEquals("No se encontro el numero buscado",buscado, agarrado);
	}
	
	public void testDelete1(){
		setupEscenario2();
		tabla.delete(3);
		assertNull("El objeto tiene que ser nulo", tabla.get(3));
		assertEquals(49, tabla.darNumEl());
	}
	
	
}
