package Test;
import Estructuras.ArbolRojoNegro;
import junit.framework.TestCase;

public class ArbolRojoNegroTest extends TestCase {
	private ArbolRojoNegro<Integer, Integer> arbol;
	
	public void setupEscenario1(){
		arbol= new ArbolRojoNegro<Integer, Integer>();
	}
	
	public void setupEscenario2(){
		arbol= new ArbolRojoNegro<Integer, Integer>();
		for (int i = 0; i < 10; i++) {
			arbol.put(i, i);
		}
	}
	
	public void testIsEmpty(){
		setupEscenario1();
		assertEquals(true, arbol.isEmpty());
	}
	
	public void testAdd(){
		setupEscenario2();
		arbol.put(60, 60);
		assertEquals(11, arbol.size(arbol.getRaiz()));
	}
	
	public void testAdd2(){
		setupEscenario1();
		arbol.put(5, 5);
		arbol.put(1, 1);
		assertEquals(2, arbol.size(arbol.getRaiz()));
	}
	
	public void testDelete(){
		setupEscenario2();
		arbol.deleteMax();
		assertEquals(10, arbol.size(arbol.getRaiz()));
	}
}
