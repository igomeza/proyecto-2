package Test;

import Estructuras.ArbolBalanceado;
import junit.framework.TestCase;

public class ArbolBalanceadoTest extends TestCase {
	private ArbolBalanceado<Integer, Integer> arbol;
	
	public void setupEscenario1(){
		arbol= new ArbolBalanceado<Integer, Integer>();
	}
	
	public void setupEscenario2(){
		arbol= new ArbolBalanceado<Integer, Integer>();
		for (int i = 0; i < 10; i++) {
			arbol.add(i, i);
		}
	}

	public void setupEscenario3(){
		arbol= new ArbolBalanceado<Integer, Integer>();
		for (int i = 0; i < 50; i++) {
			arbol.add(i, i);
		}
	}
	
	public void testIsEmpty(){
		setupEscenario1();
		assertEquals(true, arbol.isEmpty());
	}
	
	public void testAdd(){
		setupEscenario2();
		arbol.add(60, 60);
		assertEquals(1, arbol.size(arbol.getRaiz()));
	}
}
